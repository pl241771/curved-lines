# Imports required libraries
import numpy as np
import cv2

# Load image and make it readable
inputImage = cv2.imread("curveright.png")
inputImageGray = cv2.cvtColor(inputImage, cv2.COLOR_BGR2GRAY)

# Outline the edges
edges = cv2.Canny(inputImageGray, 150, 200, apertureSize=3)
minLineLength = 30
maxLineGap = 5
lines = cv2.HoughLinesP(edges, np.pi/180, 30, minLineLength, maxLineGap)
for x in range(0, len(lines)):
    for x1, y1, x2, y2 in lines[x]:
        pts = np.array([[x1, y1], [x2, y2]], np.int32)
        cv2.polylines(inputImage, [pts], True, (0, 255, 0))

# Displays the image with edges outlined
cv2.imshow('edge', edges)
cv2.waitKey(0)
